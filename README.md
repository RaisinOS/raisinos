 # About RaisinOS
 RaisinOS is a hobby OS made by two friends, xent and daveyjones01.
## Goals
Our goals with raisin is to eventually make it a 64-bit operating system. For now it is 16-bit and we plan on doing 32-bit next.
# Dependencies
1. `qemu` for booting RaisinOS.
2. `nasm` for compiling RaisinOS.
3. `xorriso` for creating the cd/dvd image of RaisinOS.
# How to build RaisinOS
1. Run `nasm -f bin -o grape.bin grape.asm` to compile `grape.asm`
2. Run `dd if=/dev/zero of=grape.img bs=1024 count=1440` to create a disk image equal to the size of a floppy
3. Run `dd if=grape.bin of=grape.img seek=0 count=1 conv=notrunc` to add the boot sector (grape.asm) to the disk image
4. Make the directory `iso/` and copy grape.img to it
5. To generate the bootable disk image using xorriso run ```genisoimage -quiet -V 'RAISIN' -input-charset iso8859-1 -o raisin.iso -b grape.img \
    -hide grape.img iso/```
6. use qemu to boot the disk image `qemu-system-i386 -cdrom ./raisin.iso`
### Disk image for lazy people
http://0x0.st/zApl.iso

# Contact us
Contact us by making an issue or messaging us on discord 
* @daveyjones01#5397 
* @xent#7467 
