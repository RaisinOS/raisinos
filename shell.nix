 let
   pkgs = import <nixpkgs> {}; 
   stdenv = pkgs.stdenv;
 in with pkgs; {
   myProject = stdenv.mkDerivation {
     name = "raisin";
     version = "1";
     buildInputs =  [
       qemu
       xorriso
       nasm
    ];
  };
}
